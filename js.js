function createTask(){
    var test = document.getElementById('text-field').value;
    if(test !== ''){
        document.getElementById('taskContainer').appendChild(createInsertElement(test, 'p'));
    } else {
        alert('enter task');
    }
}

function createInsertElement(content, htmlElem, className){
    var insertElem = document.createElement(htmlElem);
    insertElem.innerHTML = content;
    return insertElem;
}